import java.util.ArrayList;
import java.util.Collections;  // Import the Collections class
public class Task5140 {
    public static void main(String[] args) throws Exception {
        //task 1
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(1);
        numbers.add(10);
        numbers.add(-8);
        numbers.add(21);
        numbers.add(0);
        numbers.add(72);
        numbers.add(-7);
        numbers.add(6);
        numbers.add(-3);
        numbers.add(14);
        System.out.println("Task 1 - Original array: " + numbers);
        Collections.sort(numbers);
        System.out.println("Task 1 - Sorted array: " + numbers);
        //task 2
        ArrayList<Integer> arrayTask2 = new ArrayList<Integer>();
        for (int i=0; i< numbers.size(); i++){
            if (numbers.get(i)>=10 && numbers.get(i)<=100){
                arrayTask2.add(numbers.get(i));
            }
        }
        System.out.println("Task 2 - Array with element >10 and <100: " + arrayTask2);
        //task 3
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("white");
        colors.add("red");
        colors.add("blue");
        colors.add("orange");
        colors.add("pink");
        System.out.println("Task 3: " + colors);
        int vCheck = colors.indexOf("yellow");
        if (vCheck == -1){
            System.out.println("Task 3 check yellow: " + "KO");
        }
        else System.out.println("Task 3 check yellow: " + "OK");
        //task 4
        int sum =0;
        for (int i=0; i<numbers.size(); i++){
            sum += numbers.get(i);
        }
        System.out.println("Task 4 sum of array elements: " + sum);    
        //task 5
        int len = colors.size();
        while( len > 0){
            colors.remove(0);//xóa phần tử đầu tiên của array
            len = len-1;
        }
        System.out.println("Task 5 array after removing all elements: " + colors);
        //task 6
        ArrayList<String> colors6 = new ArrayList<String>();
        colors6.add("white");
        colors6.add("red");
        colors6.add("blue");
        colors6.add("orange");
        colors6.add("pink");
        colors6.add("yellow");
        colors6.add("green");
        colors6.add("black");
        colors6.add("purple");
        colors6.add("grey");
        System.out.println("Task 6 array of 10 colors: " + colors6);
        Collections.shuffle(colors6);
        System.out.println("Task 6 array of 10 colors - shuffle: " + colors6);
        //task 7
        Collections.reverse(colors6);
        System.out.println("Task 7 array of 10 colors - reverse: " + colors6);
        //task 8
        ArrayList<String> colors8 = new ArrayList<String>();
        for (int i=2; i<7; i++){
            colors8.add(colors6.get(i));
        }
        System.out.println("Task 8 array of 3rd-7th colors: " + colors8);
        //task 9
        Collections.swap(colors6,2,6);
        System.out.println("Task 9 array after swap 3rd and 7th colors: " + colors6);
        //task 10
        ArrayList<Integer> array1 = new ArrayList<Integer>();//tạo array thứ 1
        array1.add(1);
        array1.add(10);
        array1.add(-8);
        System.out.println("Task 10 array 1: " + array1);
        ArrayList<Integer> array2 = new ArrayList<Integer>();//tạo array thứ 2
        array2.add(-21);
        array2.add(18);
        array2.add(5);
        System.out.println("Task 10 array 2: " + array2);
        for (int i = 0; i < array2.size(); i++) {
            array2.set(i,array1.get(i));
        }
        System.out.println("Task 10 array 2 after copy from array1: " + array2);

    }
}
